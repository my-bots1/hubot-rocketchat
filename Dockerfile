FROM debian:10

RUN apt -qq update  && \
    apt -qq --no-install-recommends install software-properties-common curl git npm -y && \
    curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt install nodejs  -y && \
    git clone https://github.com/RocketChat/hubot-rocketchat-boilerplate /hubot-rocketchat && \
    cd hubot-rocketchat && \
    npm install
RUN apt -qq --no-install-recommends install nano
RUN apt -f install -y
WORKDIR /hubot-rocketchat
